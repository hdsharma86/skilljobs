export const config = {

    apiUrl: 'http://localhost:8080/api/v1',

    ROLE_ADMIN : 'ADMIN',
    ROLE_AGENCY : 'AGENCY',
    ROLE_TC : 'TC',
    ROLE_CANDIDATE : 'CANDIDATE'
       
};