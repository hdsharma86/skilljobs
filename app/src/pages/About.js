import React, { Component } from 'react';
import { Navigation } from '../components/Navigation/Navigation';
import Footer from '../components/Footer/Footer';
import { Model } from '../components/Model/Model';
import './style.css';

class About extends Component {

  componentDidMount(){
    document.getElementById('loader').style.display = 'none';
  }
  
  render() {
    return (
      <div className="About">
        <Navigation />
        <div class="container" style={{paddingTop:100, paddingBottom:50}}>
          <h1 class="my-4">Who We Are</h1>
          <div class="row">
            <div class="col-lg-12 mb-12">
              <div class="card h-100">
                <div class="card-body">
                  <p class="card-text"> Nunc luctus, elit at tempus porttitor, elit neque tempor erat, quis feugiat erat nibh ac ipsum. Sed sit amet nisi id mi cursus commodo vel at magna. Praesent eu consequat nisl, ac pellentesque risus. Etiam eu odio mi. Etiam facilisis odio ipsum, vel elementum justo feugiat ut. Vestibulum tincidunt rhoncus nibh, a vestibulum enim sollicitudin ut. Nullam sit amet dolor ut neque malesuada porta in ac urna. Donec hendrerit et mauris ac pellentesque. In et gravida mi.

Donec condimentum ante et enim tempus congue. Aenean pulvinar scelerisque turpis, sit amet luctus arcu dignissim nec. Curabitur imperdiet commodo ultricies. Suspendisse feugiat blandit hendrerit. Cras gravida bibendum est, eu lobortis dolor facilisis ac. Duis eget nibh gravida, pretium erat et, pretium felis. Donec elit ex, pellentesque et massa ac, lobortis tristique felis. Integer eget scelerisque urna. Nam aliquet velit orci, in volutpat est consequat eu. Sed eget aliquet odio, sed semper diam. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nulla facilisi.

Praesent pulvinar, nisi et egestas gravida, leo ligula tincidunt diam, ut porttitor elit leo at nibh. Curabitur in nibh ultrices, scelerisque nulla et, rhoncus quam. Donec porttitor leo mi, ut lobortis ex mattis non. Aliquam tincidunt neque enim, varius posuere dolor pretium sed. Praesent vel facilisis nibh. Integer ac enim eu odio cursus auctor. Integer accumsan volutpat velit ut vehicula. Nam elementum nisl nulla, at suscipit enim sodales nec. Vivamus augue neque, blandit vitae tristique quis, fringilla porttitor arcu. Fusce sagittis ultrices dui nec malesuada. Fusce feugiat scelerisque tortor, et pulvinar libero euismod vel.

Pellentesque vulputate lacinia dui, et venenatis est eleifend vel. In porta tellus felis, a scelerisque ante rutrum ut. Ut tristique hendrerit neque, at imperdiet orci semper sit amet. Nam pulvinar facilisis sapien, ut laoreet felis. Phasellus scelerisque arcu eu lectus rutrum vulputate. Maecenas scelerisque nunc et dolor blandit, lacinia tincidunt mauris convallis. Fusce tristique finibus elit at vehicula. Nulla sollicitudin justo eget efficitur sodales. Quisque id lorem enim. Quisque posuere, lorem eu imperdiet maximus, massa lorem finibus enim, ut malesuada neque erat vitae sem. Curabitur a lacus massa. Phasellus eu ex vitae ex tincidunt lobortis a eu ante. Proin et enim tristique, consequat neque nec, ornare metus. Mauris consectetur, libero vel fermentum luctus, massa magna feugiat purus, eget pellentesque arcu neque vel nisi. </p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <Footer />
        <Model />
      </div>
    );
  }
}

export default About;