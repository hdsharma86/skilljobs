import React, { Component } from 'react';
import { Navigation } from '../components/Navigation/Navigation';
import Footer from '../components/Footer/Footer';
import './style.css';

class Dashboard extends Component {
  
  componentDidMount(){
    document.getElementById('loader').style.display = 'none';
  }

  render() {
    return (
      <div className="Dashboard">
        <Navigation />
        <div class="page-container">
        <div class="card">
          <div class="card-header pull-left">
            Posted Jobs
            <div class="">Hi</div>
            <a href="#" class="btn btn-primary">Go somewhere</a>
          </div>
          <div class="card-body">
          <table class="table table-bordered">
          <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">First</th>
              <th scope="col">Last</th>
              <th scope="col">Handle</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th scope="row">1</th>
              <td>Mark</td>
              <td>Otto</td>
              <td>@mdo</td>
            </tr>
            <tr>
              <th scope="row">2</th>
              <td>Jacob</td>
              <td>Thornton</td>
              <td>@fat</td>
            </tr>
            <tr>
              <th scope="row">3</th>
              <td colspan="2">Larry the Bird</td>
              <td>@twitter</td>
            </tr>
          </tbody>
        </table>
          </div>
        </div>
        <br />
        <div class="card">
          <div class="card-header">
            Recommendation(s)
          </div>
          <div class="card-body">
          <table class="table table-bordered">
          <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">First</th>
              <th scope="col">Last</th>
              <th scope="col">Handle</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th scope="row">1</th>
              <td>Mark</td>
              <td>Otto</td>
              <td>@mdo</td>
            </tr>
            <tr>
              <th scope="row">2</th>
              <td>Jacob</td>
              <td>Thornton</td>
              <td>@fat</td>
            </tr>
            <tr>
              <th scope="row">3</th>
              <td colspan="2">Larry the Bird</td>
              <td>@twitter</td>
            </tr>
          </tbody>
        </table>
          </div>
        </div>
        </div>
        <Footer />
      </div>
    );
  }
}

export default Dashboard;
