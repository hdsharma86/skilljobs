import React, { Component } from 'react';
import { connect } from 'react-redux';
import './Navigation.css';

class Navigation extends Component {

  constructor(props) {
      super(props);
  }

  handleLogout(){
    window.localStorage.removeItem('user');
    window.location.reload(true);
  }

  render() {
    var user = window.localStorage.getItem('user');
    var authLink = '';
    if (user) {
      authLink = <a class="nav-link" onClick={this.handleLogout} href="javascript: void(0);">LOGOUT</a>;
    } else {
      authLink = <a class="nav-link" data-toggle="modal" data-target="#exampleModal" href="javascript: void(0);">SIGN IN</a>;
    }
    return (
      <nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-dark fixed-top">
      <div class="container">
        <a class="navbar-brand" href="/">Skill Jobs</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link" href="/about-us">ABOUT</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="/contact-us">CONTACT</a>
            </li>

            <li class="nav-item">
              {authLink}
            </li>
          </ul>
        </div>
      </div>
    </nav>
    );
  }
}


function mapStateToProps(state) {
  const { isLoggedIn } = state.isLoggedIn;
  return {
    isLoggedIn
  };
}

const connectedLogin = connect(mapStateToProps)(Navigation);
export { Navigation }; 
