import React, { Component } from 'react';
import { Router, Route } from 'react-router-dom';
import { connect } from 'react-redux';
import { history } from './helpers';
import { PrivateRoute } from './PrivateRoute';

import { alertActions } from './actions';

import Home from './pages/Home';
import Dashboard from './pages/Dashboard';
import About from './pages/About';
import Contact from './pages/Contact';
import './App.css';

class App extends Component {

  constructor(props) {
    super(props);

    const { dispatch } = this.props;
    history.listen((location, action) => {
      // clear alert on location change
      dispatch(alertActions.clear());
    });
  }


  render() {
    const { alert } = this.props;
    return (
      <div>
          {alert.message &&
              <div className={`main-alert alert ${alert.type}`}>{alert.message}</div>
          }
          <Router history={history}>
              <div>
                  <Route exact path="/" component={Home} />
                  <PrivateRoute path="/dashboard" component={Dashboard} />
                  <Route path="/about-us" component={About} />
                  <Route path="/contact-us" component={Contact} />
              </div>
          </Router>
      </div>
    );
  }
}

function mapStateToProps(state) {
  const { alert } = state;
  return {
      alert
  };
}

const connectedApp = connect(mapStateToProps)(App);
export { connectedApp as App }; 
