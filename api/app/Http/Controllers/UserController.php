<?php
namespace App\Http\Controllers;
 
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Entity\User;
use App\Entity\UserRole;
 
class UserController extends Controller{
 
  public function __construct(){

   //  $this->middleware('auth:api');

  }

  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  *
  */
  public function authenticate(Request $request){

    $this->validate($request, [
     'email' => 'required',
     'password' => 'required'
    ]);

    $user = User::where('email', $request->input('email'))->first();
    $roles = User::find($user->id)->role;

    if(Hash::check($request->input('password'), $user->password)){
        $token = base64_encode(str_random(40));
        UserRole::where('user_id', $user->id)->update(['token' => "$token"]);;
        return response()->json(['status' => 'success','token' => $token, 'role'=>$roles->role]);
    } else {
        return response()->json(['status' => 'fail', 'message'=>'Username or password is incorrect'],401);
    }

  }
 
}    
 
?>