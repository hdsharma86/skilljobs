<?php

namespace App\Http\Controllers;
 
use App\Entity\Candidate;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CandidateController extends Controller{
 
	public function createCandidate(Request $request){
 
    	$candidate = Candidate::create($request->all());
 
    	return response()->json($candidate);
 
	}
 
	public function updateCandidate(Request $request, $id){
 
    	$candidate  = Candidate::find($id);
    	$candidate->first_name = $request->input('first_name');
    	$candidate->last_name = $request->input('last_name');
    	$candidate->email = $request->input('email');
    	$candidate->save();
 
    	return response()->json($candidate);
	}  
 
	public function deleteCandidate($id){
    	$candidate  = Candidate::find($id);
    	$candidate->delete();
 
    	return response()->json('Removed successfully.');
	}
 
	public function index(){
 
    	$candidates  = Candidate::all();
    	return response()->json($candidates);
 
	}
}
?>