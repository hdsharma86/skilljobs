<?php

namespace App\Entity;
 
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Auth\Authenticatable as AuthenticableTrait;

class User extends Model implements Authenticatable{
 
	protected $table = 'skilljobs.users';

	//
	use AuthenticableTrait;
	protected $fillable = ['email','password'];
	protected $hidden = [
	  'password'
	];
 
 	public function role(){
 		return $this->hasOne('App\Entity\UserRole', 'user_id');
 	}
}