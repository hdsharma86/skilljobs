<?php

namespace App\Entity;
 
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Auth\Authenticatable as AuthenticableTrait;

class UserRole extends Model implements Authenticatable{
 
	protected $table = 'skilljobs.users_roles';

	use AuthenticableTrait;
	protected $fillable = ['user_id','role','token'];
	protected $hidden = [
	  'token'
	];
 
 	public function user()
    {
        return $this->belongsTo('App\Entity\User', 'user_id');
    }
}