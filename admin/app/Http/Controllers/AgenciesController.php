<?php

namespace App\Http\Controllers;

use App\Agency;
use Illuminate\Http\Request;

class AgenciesController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $agencies = Agency::latest()->paginate(10);
        return view('agencies.index', compact('agencies'))->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('agencies.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // validate request
        $this->validate($request, [
            'name' => 'required|max:100',
            'agency_contact' => 'required',
            'agency_city' => 'required',
            'agency_address' => 'required',
        ]);

        $agency = new Agency;
        $agency->agency_name = $request->name;
        $agency->agency_contact = $request->agency_contact;
        $agency->agency_location = $request->agency_city;
        $agency->agency_address = $request->agency_address;
        $agency->save();
        return redirect()->route('agencies.index')
                        ->with('success', 'Agency created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Agency  $agency
     * @return \Illuminate\Http\Response
     */
    public function show(Agency $agency)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Agency  $agency
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
         $agency = Agency::find($id);
        return view('agencies.edit', compact('agency'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Agency  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request, [
            'agency_name' => 'required|max:100',
            'agency_contact' => 'required',
            'agency_location' => 'required',
            'agency_address' => 'required',
        ]);
        Agency::find($id)->update($request->all());
        return redirect()->route('agencies.index')
                        ->with('success', 'Agency updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Agency  $Id
     * @return \Illuminate\Http\Response
     */
    public function destroy($Id)
    {
        //
        Agency::find($Id)->delete();
        return redirect()->route('agencies.index')
                        ->with('success', 'Agency deleted successfully');
    }

}
