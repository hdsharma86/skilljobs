<?php

namespace App\Http\Controllers;

use App\TrainingCenter;
use Illuminate\Http\Request;

class TrainingCentersController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tc = TrainingCenter::latest()->paginate(10);
        return view('tc.index', compact('tc'))->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('tc.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        // validate request
        $this->validate($request, [
            'contact_person_name' => 'required|max:100',
            'user_id' => 'required|integer',
            'contact' => 'required',
            'address' => 'required',
        ]);

        $agency = new TrainingCenter;
        $agency->user_id = $request->user_id;
        $agency->contact_person_name = $request->contact_person_name;
        $agency->contact = $request->contact;
        $agency->address = $request->address;
        $agency->save();
        return redirect()->route('tc.index')
                        ->with('success', 'Training Center created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TrainingCenter  $trainingCenter
     * @return \Illuminate\Http\Response
     */
    public function show(TrainingCenter $trainingCenter)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TrainingCenter  $trainingCenter
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $tc = TrainingCenter::find($id);
        return view('tc.edit', compact('tc'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TrainingCenter  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request, [
            'contact_person_name' => 'required|max:100',
            'user_id' => 'required|integer',
            'contact' => 'required',
            'address' => 'required',
        ]);
        TrainingCenter::find($id)->update($request->all());
        return redirect()->route('tc.index')
                        ->with('success', 'Training Center updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TrainingCenter  $trainingCenter
     * @return \Illuminate\Http\Response
     */
    public function destroy($Id)
    {
        //
        TrainingCenter::find($Id)->delete();
        return redirect()->route('tc.index')
                        ->with('success', 'Training Center deleted successfully');
    }

}
