<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TrainingCenter extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'training_centers';
    protected $fillable = ['user_id', 'contact_person_name', 'contact', 'address', 'created_at', 'updated_at'];

}
