<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Agency extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'agencies';
    protected $fillable = ['agency_name', 'agency_contact', 'agency_location', 'agency_address', 'created_at', 'updated_at'];

}
