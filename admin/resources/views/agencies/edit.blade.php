@extends('layouts.app')

@section('title', '| Edit Agency')

@section('content')

<section class="content-header">
    <h1>Agency</h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Agency</a></li>
        <li class="active">Edit</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-6">
            @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <h1><i class='fa fa-user-plus'></i> Edit Agency</h1>
            <hr>
            {!! Form::model($agency, ['method' => 'PATCH','route' => ['agencies.update', $agency->id]]) !!}
            <div class="form-group">
                {{ Form::label('agency_name', 'Agency Name') }}
                {{ Form::text('agency_name', null, array('class' => 'form-control')) }}
            </div>

            <div class="form-group">
                {{ Form::label('agency_contact', 'Agency Contact') }}
                {{ Form::text('agency_contact', null, array('class' => 'form-control')) }}
            </div>

            <div class="form-group">
                {{ Form::label('agency_location', 'Agency City') }}
                {{ Form::text('agency_location', null, array('class' => 'form-control')) }}
            </div>

            <div class="form-group">
                {{ Form::label('agency_address', 'Agency Address') }}
                {{ Form::text('agency_address', null, array('class' => 'form-control')) }}
            </div>

            {{ Form::submit('Update', array('class' => 'btn btn-primary')) }}
            {!! Form::close() !!}

        </div>
    </div>
</section>

@endsection