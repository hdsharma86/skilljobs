@extends('layouts.app')

@section('title', '|  Agencies')

@section('content')
<section class="content-header">
    <h1>
        Agencies
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{ route('agencies.index') }}">Agencies</a></li>
        <li class="active">List</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <p>{{ $message }}</p>
                </div>
            @endif
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Manage Agencies</h3>

              <div class="box-tools">
                <div class="input-group input-group-sm">
                  <a href="{{ route('agencies.create') }}" class="btn btn-success"><i class="fa fa-plus"></i></a>
                </div>
                 
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tr>
                    <th>Name</th>
                    <th>Contact</th>
                    <th>City</th>
                    <th>Address</th>
                    <th>Date/Time Added</th>
                    <th>Operations</th>
                </tr>
                @foreach ($agencies as $agency)
                <tr>

                    <td>{{ $agency->agency_name }}</td>
                    <td>{{ $agency->agency_contact }}</td>
                    <td>{{ $agency->agency_location  }}</td>
                    <td>{{ $agency->agency_address  }}</td>
                    <td>{{ $agency->created_at->format('F d, Y h:ia') }}</td>
                    <td>
                        <a href="{{ route('agencies.edit', $agency->id) }}" class="pull-left" style="margin-right: 3px;"><i class="fa fa-edit"></i></a>

                        {!! Form::open(['method' => 'DELETE', 'route' => ['agencies.destroy', $agency->id] ]) !!}
                        {!! Form::button('<a class="fa fa-trash-o"></a>', ['style' => 'padding:0px;margin:0px;border:0;background:none;']) !!}
                        {!! Form::close() !!}
                        

                    </td>
                </tr>
                @endforeach
              </table>
              
            </div>
            <!-- /.box-body -->
            <div class="box-footer" style="">
                {!! $agencies->render() !!}
            </div>
          </div>
          <!-- /.box -->
        </div>
      </div>
</section>
@endsection