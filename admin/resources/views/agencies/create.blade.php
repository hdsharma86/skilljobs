@extends('layouts.app')

@section('title', '| Add Agency')

@section('content')

<section class="content-header">
    <h1>Agency</h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Agency</a></li>
        <li class="active">Add</li>
    </ol>
</section>
<section class="content">

    <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title"><i class='fa fa-user-plus'></i> Add New Agency</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            {{ Form::open(array('route' => 'agencies.store')) }}
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label>Agency Name</label>
                {{ Form::text('name', '', array('class' => 'form-control')) }}
                @if($errors->has('name'))
                    <p class="error">{{ $errors->first('name') }}</p>
                @endif
              </div>
              <!-- /.form-group -->
              <div class="form-group">
                <label>City</label>
                {{ Form::text('agency_city', '', array('class' => 'form-control')) }}
                @if($errors->has('agency_city'))
                    <p class="error">{{ $errors->first('agency_city') }}</p>
                @endif
              </div>
              <!-- /.form-group -->
            </div>
            <!-- /.col -->
            <div class="col-md-6">
              <div class="form-group">
                <label>Agency Contact</label>
                {{ Form::text('agency_contact', '', array('class' => 'form-control')) }}
                @if($errors->has('agency_contact'))
                    <p class="error">{{ $errors->first('agency_contact') }}</p>
                @endif
              </div>
              <!-- /.form-group -->
              <div class="form-group">
                <label>Address</label>
                {{ Form::text('agency_address', '', array('class' => 'form-control')) }}
                @if($errors->has('agency_address'))
                    <p class="error">{{ $errors->first('agency_address') }}</p>
                @endif
              </div>
              <!-- /.form-group -->
            </div>
            <!-- /.col -->

            <div class="col-md-12">
              <div class="form-group">
                {{ Form::submit('Save', array('class' => 'btn btn-primary')) }}
              </div>
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
          {{ Form::close() }}
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          Add New Agency
        </div>
      </div>
      <!-- /.box -->
</section>

@endsection