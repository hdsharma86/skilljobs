<!DOCTYPE html>
<html>
    <head>
        @include('includes.head')
    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">


            @include('includes.header')
            @include('includes.sidebar')

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                @yield('content')
            </div>
            <!-- /.content-wrapper -->
            <footer class="main-footer">
                <div class="pull-right hidden-xs">
                    <b>Version</b> 2.4.0
                </div>
                <strong>Copyright &copy; 2014-2018 <a href="https://adminlte.io">Skill2Jobs</a>.</strong> All rights
                reserved.
            </footer>

        </div>
        <!-- ./wrapper -->
        @include('includes.footer')
    </body>
</html>