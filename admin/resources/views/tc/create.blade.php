@extends('layouts.app')

@section('title', '| Add Training Center')

@section('content')

<section class="content-header">
    <h1>Agency</h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Training Center</a></li>
        <li class="active">Add</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-6">
            @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <h1><i class='fa fa-user-plus'></i> Add Training Center</h1>
            <hr>

            {{ Form::open(array('route' => 'tc.store')) }}
            <div class="form-group">
                {{ Form::label('user_id', 'User ID') }}
                {{ Form::text('user_id', '', array('class' => 'form-control')) }}
            </div>

            <div class="form-group">
                {{ Form::label('contact_person_name', 'Contact Person') }}
                {{ Form::text('contact_person_name', '', array('class' => 'form-control')) }}
            </div>

            <div class="form-group">
                {{ Form::label('contact', 'Contact No.') }}
                {{ Form::text('contact', '', array('class' => 'form-control')) }}
            </div>
            
            <div class="form-group">
                {{ Form::label('address', 'Training Center Address') }}
                {{ Form::text('address', '', array('class' => 'form-control')) }}
            </div>

            {{ Form::submit('Add', array('class' => 'btn btn-primary')) }}

            {{ Form::close() }}

        </div>
    </div>
</section>

@endsection