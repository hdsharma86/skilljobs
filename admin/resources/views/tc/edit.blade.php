@extends('layouts.app')

@section('title', '| Edit Training Center')

@section('content')

<section class="content-header">
    <h1>Training Center</h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Training Center</a></li>
        <li class="active">Edit</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-6">
            @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <h1><i class='fa fa-user-plus'></i> Edit Training Center</h1>
            <hr>
            {!! Form::model($tc, ['method' => 'PATCH','route' => ['tc.update', $tc->id]]) !!}
            <div class="form-group">
                {{ Form::label('user_id', 'User Id') }}
                {{ Form::text('user_id', null, array('class' => 'form-control')) }}
            </div>

            <div class="form-group">
                {{ Form::label('contact_person_name', 'Contact Person Name') }}
                {{ Form::text('contact_person_name', null, array('class' => 'form-control')) }}
            </div>

            <div class="form-group">
                {{ Form::label('contact', 'Contact') }}
                {{ Form::text('contact', null, array('class' => 'form-control')) }}
            </div>

            <div class="form-group">
                {{ Form::label('address', 'Address') }}
                {{ Form::text('address', null, array('class' => 'form-control')) }}
            </div>

            {{ Form::submit('Update', array('class' => 'btn btn-primary')) }}
            {!! Form::close() !!}

        </div>
    </div>
</section>

@endsection