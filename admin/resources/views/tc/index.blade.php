@extends('layouts.app')

@section('title', '|  Training Centers')

@section('content')
<section class="content-header">
    <h1>
        Training Centers
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{ route('tc.index') }}">Training Centers</a></li>
        <li class="active">List</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
            @endif
            <div class="table-responsive">
                <table class="table table-bordered table-striped">

                    <thead>
                        <tr>
                            <th>User Id</th>
                            <th>Contact Person Name</th>
                            <th>Contact</th>
                            <th>Address</th>
                            <th>Date/Time Added</th>
                            <th>Operations</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach ($tc as $tc_item)
                        <tr>

                            <td>{{ $tc_item->user_id }}</td>
                            <td>{{ $tc_item->contact_person_name }}</td>
                            <td>{{ $tc_item->contact  }}</td>
                            <td>{{ $tc_item->address  }}</td>
                            <td>{{ $tc_item->created_at->format('F d, Y h:ia') }}</td>
                            <td>
                                <a href="{{ route('tc.edit', $tc_item->id) }}" class="btn btn-info pull-left" style="margin-right: 3px;">Edit</a>

                                {!! Form::open(['method' => 'DELETE', 'route' => ['tc.destroy', $tc_item->id] ]) !!}
                                {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                                {!! Form::close() !!}

                            </td>
                        </tr>
                        @endforeach
                    </tbody>

                </table>
                {!! $tc->render() !!}
            </div>

            <a href="{{ route('tc.create') }}" class="btn btn-success">Add Training Center</a>
        </div>
    </div>
</section>
@endsection